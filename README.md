# Checkstyle Linting

## Add Checkstyle Plugin to gradle

In your build.gradle file add the checkstyle plugin

```groovy
plugins {
	// your other plugins
	id 'checkstyle'
}

// we need to add to the checkstyle task
checkstyle {
	toolVersion '8.27'
	configFile file("checkstyle.xml"
}
```

## Add checkstyle.xml to project

At the root of your project add the XML of your desired Java style guide. We will be using the Google Java Style guide throughout this class. Look at the included checkstyle.xml file in this repo for an example.

## Run gradle check

Using the gradle wrapper of your project run `./gradlew check --stacktrace` to see the linter in action.

Checkstyle also should generate a linting report at `project-root/reports/checkstyle/main.html` which you can load in a browser

